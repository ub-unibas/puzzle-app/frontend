/** @type {import('tailwindcss').Config} */
export default {
    content: ["./src/**/*.{html,js,svelte,ts}"],
    darkMode: "class",
    theme: {
        extend: {
            colors: {
                "light-grayish-yellow": "#dfdac4",
                "unibas-mint": "#a5d7d2",
                "unibas-dark-gray": "#2d373c",
                "unibas-dark-gray-contrast": "#b0b4b4",
                "unibas-red": "#d20537",
                "unibas-light-gray": "#eaebec",
                "unibas-font": "#eeeeee",
            },
            animation: {
                "attach-pieces": "brighten 0.5s linear 1",
                wiggle: "wiggle 12s linear infinite",
            },
            keyframes: {
                brighten: {
                    "20%": { filter: "brightness(120%)" },
                },
                wiggle: {
                    "0%, 3.5%": { transform: "rotate(0deg)" },
                    "0.5%": { transform: "rotate(-6deg)" },
                    "1.5%": { transform: "rotate(6deg)" },
                    "2.5%": { transform: "rotate(-3deg)" },
                },
            },
            gridTemplateColumns: {
                "flex-cols": "repeat( auto-fit, minmax(100px, 1fr))",
                "two-cols": "repeat( 2, minmax(200px, 1fr))",
                "four-cols": "repeat( 4, minmax(200px, 1fr))",
            },
        },
    },
    plugins: [require("@tailwindcss/forms")],
};
