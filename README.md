# Puzzle App: Frontend

The Puzzle App is a web based application for playing jigsaw puzzles. Its hallmark feature is the support
of [IIIF manifests](https://iiif.io/api/presentation/3.0/) as a way of importing new images and their respective
metadata.

This component (the frontend) is designed to work with the latest version of
the [Puzzle App's backend](https://gitlab.switch.ch/ub-unibas/puzzle-app/backend).

## Configuration

The frontend is configured via environment variables. They must be present during the building step. It is recommended
to add them via a `.env`, `.env.local`, `.env.[mode]` or `.env.[mode].local` file in the root folder (see the [Vite build tool guide](https://vitejs.dev/guide/env-and-mode#env-files) for more information).

```bash
PUBLIC_BACKEND_URI=http://localhost:3000 # Address of the backend component
PUBLIC_INSTITUTION_URI=                  # Link to your institution's home page. If not empty, link is shown in epilogue
PUBLIC_CLEAR_TABLE_BORDER_DISTANCE=50    # Pieces' maximal distance (in px) from border when "clearing" the puzzle table
PUBLIC_DARK_MODE=0                       # Light (`0`) or dark (`1`) mode set. If set to `auto`, the browser's mode is set. If set to `manual`, the mode can be switched.
PUBLIC_DISABLE_EXTERNAL_LINKS=0          # Disable external links in UI and metadata
PUBLIC_LANGUAGE_DEFAULT=de               # Default language (see [language section](#language) for more information)
PUBLIC_LANGUAGE_SWITCH=1                 # Enable (`1`) or disable (`0`) language dialog
PUBLIC_TITLE="Puzzle App"                # Title of the app (also used for Open Graph title)
PUBLIC_DESCRIPTION="A short descr"       # App's description (also used for Open Graph description)
PUBLIC_OG_IMAGE=static/social_pre.png    # Open Graph: Link to the preview image
PUBLIC_OG_URL=https://localhost:9000     # Open Graph: Root url of application
PUBLIC_RANK_LIST_LENGTH=10               # Show first n entries in rankings
PUBLIC_SHOW_DISCLAIMER=1                 # Show (`1`) or hide (`0`) disclaimer
PUBLIC_SNAP_AREA=10                      # Distance (in px) when two adjacent pieces snap
PUBLIC_SNAP_ON_APPROACH=1                # Should pieces snap automatically (`1`) or only after releasing a piece (`0`)?
```

## Usage

## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
# Open running instance
npm run dev               # or pnpm, yarn

# or start the server and open the app in a new browser tab
npm run dev -- --open     # or pnpm, yarn
```

In order to use the app, a running backend server instance is needed as well.

## Building

To create a production version of your app:

```bash
npm run build             # or pnpm, yarn
```

You can preview the production build with `npm run preview`.

> To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.
