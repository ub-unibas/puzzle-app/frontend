import { writable } from "svelte/store";
import { PUBLIC_LANGUAGE_DEFAULT } from "$env/static/public";

/** @type Writable<number> */ export const puzzleId = writable(null);
/** @type Writable<string> */ export const puzzleResolutionId = writable(null);
/** @type Writable<object> */ export const puzzleData = writable(null);
/** @type Writable<number> */ export const gameTime = writable(null);
/** @type Writable<string> */ export const gameRank = writable(null);

/** @type Writable<boolean> */ export const smallPiecesInFrontOption =
  writable(false);
/** @type Writable<boolean> */ export const darkMode = writable(null);
/** @type Writable<string> */ export const largeThumbnailPath = writable(null);
export const language = writable(PUBLIC_LANGUAGE_DEFAULT);
