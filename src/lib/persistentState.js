import {
  PUBLIC_BACKEND_URI,
  PUBLIC_SHOW_DISCLAIMER,
  PUBLIC_LANGUAGE_SWITCH,
} from "$env/static/public";
import { writable } from "svelte/store";

export const backendUri = PUBLIC_BACKEND_URI.endsWith("/")
  ? PUBLIC_BACKEND_URI.substring(0, -1)
  : PUBLIC_BACKEND_URI;
export const apiPath = "api";
export const puzzlesPath = "puzzles";
export const enableSwitchLanguage = PUBLIC_LANGUAGE_SWITCH === "1";

export const showDisclaimer = writable(PUBLIC_SHOW_DISCLAIMER === "1");

export const puzzleList = writable(null);
/** @type Writable<string> */ export const puzzleSize = writable(null);

export async function getAllImages(puzzlesRootUri, puzzlesPath) {
  return fetch(`${puzzlesRootUri}/${puzzlesPath}/index.json`)
    .then((res) => {
      /*if (!res.ok) {
                throw new Error(`HTTP error: ${res.status}`)
            }*/
      return res.json();
    })
    .then((json) => {
      return json;
    });
}

export async function getImageInfo(
  puzzlesRootUri,
  puzzlesPath,
  imageId,
  resolution,
) {
  return fetch(
    `${puzzlesRootUri}/${puzzlesPath}/${imageId}/${resolution}/index.json`,
  )
    .then((res) => {
      if (!res.ok) {
        throw new Error(`HTTP error: ${res.status}`);
      }
      return res.json();
    })
    .then((json) => {
      return json;
    });
}
