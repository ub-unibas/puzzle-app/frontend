import { getDynamicString, getI18NString } from "./language.js";

function mayBeI18String(obj, fallbackMsg) {
  let i18NObj;
  if (typeof obj === "string") {
    i18NObj = { none: [obj] };
  } else if (typeof obj === "object") {
    i18NObj = obj;
  }
  return getI18NString(i18NObj, fallbackMsg);
}

export const getSummary = (index, fallbackMsg) =>
  getDynamicString(index, (obj) => obj.metadata.summary, fallbackMsg);
export const getLabel = (index, fallbackMsg) =>
  getDynamicString(index, (obj) => obj.metadata.label, fallbackMsg);
export const getRights = (index, fallbackMsg) =>
  "rights" in index.metadata ? index.metadata.rights : fallbackMsg;
export const getMetadata = (index, fallbackMsg) =>
  "metadata" in index.metadata
    ? index.metadata.metadata
        .map((e) => [
          mayBeI18String(e.label, undefined),
          mayBeI18String(e.value, undefined),
        ])
        .filter((e) => e[0] && e[1])
    : fallbackMsg;
export const getRequiredStatement = (index, fallbackMsg) => {
  if ("requiredStatement" in index.metadata) {
    const rqStmt = [
      mayBeI18String(index.metadata.requiredStatement.label, undefined),
      mayBeI18String(index.metadata.requiredStatement.value.label, undefined),
    ];
    return rqStmt[0] && rqStmt[1] ? rqStmt : fallbackMsg;
  } else {
    return fallbackMsg;
  }
};
export const getFormat = (index, fallbackMsg) =>
  "format" in index.metadata ? index.metadata.format : fallbackMsg;
export const getHomepages = (index, fallbackMsg) => {
  if ("homepage" in index.metadata) {
    return index.metadata.homepage
      .map((h) => getHomepage(h))
      .filter((e) => e[1]);
  } else {
    return fallbackMsg;
  }
};
export const getSeeAlso = (index, fallbackMsg) => {
  if ("seeAlso" in index.metadata) {
    let uri = index.metadata.seeAlso.id;
    let label = mayBeI18String(index.metadata.seeAlso.label, fallbackMsg);
    return [label, uri];
  } else {
    return fallbackMsg;
  }
};
export const getProviders = (index, fallbackMsg) => {
  if ("provider" in index.metadata) {
    return index.metadata.provider.map((p) => [
      mayBeI18String(p.label),
      getAgentHomepages(p),
      "logo" in p ? p.logo.id : undefined,
    ]);
  } else {
    return fallbackMsg;
  }
};
export const getAgentHomepages = (obj) => {
  if ("homepage" in obj) {
    return obj.homepage.map((h) => getHomepage(h)).filter((e) => e[1]);
  }
};
const getHomepage = (homepage) => [
  homepage.id,
  mayBeI18String(homepage.label, undefined),
];
