import i18nDe from "$lib/assets/i18n.de.json";
import i18nEn from "$lib/assets/i18n.en.json";
import i18nFr from "$lib/assets/i18n.fr.json";
import { language } from "$lib/ephemeralState.js";

/** @type {string} */
let lang;
language.subscribe((l) => (lang = l));

export function getStaticString(getPath, fallbackMsg) {
  try {
    if (lang === "de") {
      return getPath(i18nDe);
    } else if (lang === "fr") {
      return getPath(i18nFr);
    } else if (lang === "en") {
      return getPath(i18nEn);
    }
  } catch {
    return fallbackMsg;
  }
  return fallbackMsg;
}

export function getDynamicString(o, getPath, fallbackMsg) {
  try {
    const obj = getPath(o);
    return getI18NString(obj, fallbackMsg);
  } catch {
    return fallbackMsg;
  }
}

export function getI18NString(obj, fallbackMsg) {
  if (lang in obj) {
    return Array.isArray(obj[lang]) ? obj[lang].join(" ") : obj[lang];
  } else if ("none" in obj) {
    return Array.isArray(obj["none"]) ? obj["none"].join(" ") : obj["none"];
  } else {
    return fallbackMsg;
  }
}
