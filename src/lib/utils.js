import { goto } from "$app/navigation";
import { darkMode } from "$lib/ephemeralState.js";
import { PUBLIC_DARK_MODE } from "$env/static/public";

/** @param puzzleObj {{id: number, pieces_no: number[]}} */
export function generateScoresId(puzzleObj) {
  const str = `${puzzleObj.id}-${puzzleObj.pieces_no.join("-")}`;
  let hash = 0;
  if (str.length === 0) return hash;
  for (const chr of str) {
    hash = (hash << 5) - hash + chr.charCodeAt(0);
    hash |= 0; // Convert to 32bit integer
  }
  return Math.abs(hash);
}

/** @param {KeyboardEvent} event */
export function onGlobalKey(event) {
  if (event.key === "Escape") {
    goto("/");
  }
  if (PUBLIC_DARK_MODE === "manual" && event.key === "d") {
    darkMode.update((f) => !f);
  }
}
