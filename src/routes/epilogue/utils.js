/** @param {number} secs */
export const secsToMinsAndSecs = (secs) => {
  const mins = Math.floor(secs / 60);
  return [mins, secs - 60 * mins];
};
