import { error } from "@sveltejs/kit";
import { backendUri, getAllImages, puzzlesPath } from "$lib/persistentState.js";

export const ssr = true;

/** @type {import('./$types').PageServerLoad} */
export async function load({ params }) {
  let error;
  let json = await fetch(`${backendUri}/${puzzlesPath}/index.json`)
    .catch((e) => {
      return undefined;
    })
    .then(
      (res) => {
        if (!res) {
          error = {
            title: `Puzzle App server not available on ${backendUri}`,
            subtitle: "Does the server run without problems?",
          };
          return undefined;
        } else if (!res.ok) {
          error = {
            title: `Fetching data from backend on ${backendUri} failed: ${res.statusText}`,
          };
          return undefined;
        } else {
          return res.json();
        }
      },
      /*if (!res.ok) {
                throw new Error(`HTTP error: ${res.status}`)
            }*/
    )
    .then((json) => {
      return json;
    });

  if (!json) {
    return { error };
  } else {
    return { json };
  }
}
