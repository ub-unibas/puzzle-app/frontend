import { browser } from "$app/environment";
import { getDynamicString } from "$lib/language";
import { backendUri, puzzlesPath } from "$lib/persistentState.js";
import { generateScoresId } from "$lib/utils";

export const ssr = true;

const getImages = async () => {
    return await fetch(`${backendUri}/${puzzlesPath}/index.json`)
        .catch(() => {
            return undefined;
        })
        .then(
            (res) => {
                if (!res || !res.ok) {
                    return undefined;
                } else {
                    return res.json();
                }
            },
        )
        .then((json) => json["images"].map((img) => img["uid"]));
}

/** @param {number} imageId */
const getPuzzlesWithResolutions = async (imageId) => {
    return await fetch(`${backendUri}/${puzzlesPath}/${imageId}/index.json`)
        .catch(() => {
            return undefined;
        })
        .then(
            (res) => {
                if (!res || !res.ok) {
                    return undefined;
                } else {
                    return res.json();
                }
            },
            /*if (!res.ok) {
                      throw new Error(`HTTP error: ${res.status}`)
                  }*/
        )
        .then((json) =>
            json["puzzles"].flatMap((puzzle) => {
                return puzzle["resolutions"].map((res) => {
                    return {
                        resolution: [res["dimensions"][0], res["dimensions"][1]],
                        pieces: [puzzle["size"][0], puzzle["size"][1]],
                        imageId: imageId,
                        resolutionId: res["resolution_uid"],
                        puzzlePath: `${imageId}/${puzzle["size"][0]}x${puzzle["size"][1]}/${res["resolution_uid"]}`,
                        thumbnailPath: `${backendUri}/${json["thumbnails"][0]["image_path"]}`,
                    };
                });
            }),
        );
}

/** @param {string} puzzlePath */
const getPuzzleInfo = async (puzzlePath) => {
    return await fetch(`${backendUri}/${puzzlesPath}/${puzzlePath}/index.json`)
        .catch(() => {
            return undefined;
        })
        .then(
            (res) => {
                if (!res || !res.ok) {
                    return undefined;
                } else {
                    return res.json();
                }
            },
            /*if (!res.ok) {
                      throw new Error(`HTTP error: ${res.status}`)
                  }*/
        )
        .then((json) => {
            return {
                puzzleId: json["id"],
                label: getDynamicString(
                    json,
                    (obj) => obj.metadata.label,
                    "<no title>",
                ),
            };
        });
}

/** @param {number} puzzleId
 * @param {number[]} pieces */
const getRanking = async (puzzleId, pieces) => {
    const obj = {
        id: puzzleId,
        pieces_no: pieces,
    };
    const scoreId = generateScoresId(obj);
    return await fetch(`${backendUri}/api/scores/${scoreId}`)
        .catch(() => {
            return undefined;
        })
        .then(
            (res) => {
                if (!res || !res.ok) {
                    return undefined;
                } else {
                    return res.json();
                }
            },
            /*if (!res.ok) {
                      throw new Error(`HTTP error: ${res.status}`)
                  }*/
        );
}

/** @type {import('./$types').PageLoad} */
export async function load() {
    if (browser) {
        return {
            images: await getImages().then((imgIds) =>
                imgIds.map((puzzleId) =>
                    getPuzzlesWithResolutions(puzzleId).then((puzzles) =>
                        puzzles.map((puzzle) =>
                            getPuzzleInfo(puzzle["puzzlePath"]).then((metadata) =>
                                getRanking(metadata.puzzleId, puzzle.pieces).then((ranking) => {
                                    return { ...puzzle, ...metadata, ranking: ranking };
                                }),
                            ),
                        ),
                    ),
                ),
            ),
        };
    }
}
